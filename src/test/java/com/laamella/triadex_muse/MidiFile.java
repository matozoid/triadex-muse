package com.laamella.triadex_muse;

import java.io.File;
import java.io.IOException;

import javax.sound.midi.Sequence;
import javax.sound.midi.MidiEvent;
import javax.sound.midi.MidiSystem;
import javax.sound.midi.ShortMessage;
import javax.sound.midi.Track;
import javax.sound.midi.InvalidMidiDataException;


public class MidiFile {
    private final Sequence sequence;

    public MidiFile() throws InvalidMidiDataException {
        sequence = new Sequence(Sequence.SMPTE_24, 1);
    }

    public Track addTrack() {
        return sequence.createTrack();
    }

    public void write(File file) throws IOException {
        MidiSystem.write(sequence, 1, file);
    }

    public static MidiEvent createNoteOnEvent(int key, long tick) throws InvalidMidiDataException {
        return createNoteEvent(ShortMessage.NOTE_ON, key, 64, tick);
    }


    public static MidiEvent createNoteOffEvent(int key, long tick) throws InvalidMidiDataException {
        return createNoteEvent(ShortMessage.NOTE_OFF, key, 0, tick);
    }


    public static MidiEvent createNoteEvent(int command, int key, int velocity, long tick) throws InvalidMidiDataException {
        ShortMessage message = new ShortMessage();
        // always on channel 1
        message.setMessage(command, 0, key, velocity);
        return new MidiEvent(message, tick);
    }

}

