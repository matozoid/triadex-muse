package com.laamella.triadex_muse;

import org.junit.Test;

import javax.sound.midi.InvalidMidiDataException;
import javax.sound.midi.Track;
import java.io.File;
import java.io.IOException;

import static com.laamella.triadex_muse.MidiFile.createNoteOffEvent;
import static com.laamella.triadex_muse.MidiFile.createNoteOnEvent;

public class MuseTest {
    @Test
    public void a() throws InvalidMidiDataException, IOException {
        Muse muse = new Muse();
        muse.theme[0] = 4;
        muse.theme[1] = 2;
        muse.theme[2] = 15;
        muse.theme[3] = 10;
        muse.interval[0] = 9;
        muse.interval[1] = 13;
        muse.interval[2] = 2;
        muse.interval[3] = 8;

        Muse muse1 = new Muse();
        muse1.theme[0] = 7;
        muse1.theme[1] = 5;
        muse1.theme[2] = 11;
        muse1.theme[3] = 10;
        muse1.interval[0] = 5;
        muse1.interval[1] = 9;
        muse1.interval[2] = 22;
        muse1.interval[3] = 20;

        Muse muse2 = new Muse();
        muse2.theme[0] = 5;
        muse2.theme[1] = 10;
        muse2.theme[2] = 15;
        muse2.theme[3] = 20;
        muse2.interval[0] = 5;
        muse2.interval[1] = 10;
        muse2.interval[2] = 15;
        muse2.interval[3] = 20;

        MidiFile midiFile = new MidiFile();
        Track track = midiFile.addTrack();
        Track track1 = midiFile.addTrack();
        Track track2 = midiFile.addTrack();
        int tick = 0;
        for (int i = 0; i < 160; i++) {
            int note = muse.step();
            track.add(createNoteOnEvent(60 + note, tick));
            int note1 = muse1.step();
            track1.add(createNoteOnEvent(60 + note1, tick));
            int note2 = muse2.step();
            track2.add(createNoteOnEvent(60 + note2, tick));
            tick+=10;
            track.add(createNoteOffEvent(60 + note, tick));
            track1.add(createNoteOffEvent(60 + note1, tick));
            track2.add(createNoteOffEvent(60 + note2, tick));
        }
        midiFile.write(new File("test.midi"));
    }
}

