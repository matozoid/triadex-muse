package com.laamella.triadex_muse;

import static java.lang.System.arraycopy;
import static java.util.Arrays.fill;

public class Muse {
    private int[] register = new int[40];
    private int tempoClock;
    public final int[] interval = new int[4];
    public final int[] theme = new int[4];

    public Muse() {
        reset();
    }

    public void reset() {
        fill(register, 0);
        register[1] = 1;
        tempoClock = 0;
    }

    public int step() {
        tempoClock++;
        tempoClock %= 48;

        //
        toggleRegisterWhenClockDividesBy(2, 1);
        toggleRegisterWhenClockDividesBy(3, 2);
        toggleRegisterWhenClockDividesBy(4, 4);
        toggleRegisterWhenClockDividesBy(5, 8);
        toggleRegisterWhenClockDividesBy(6, 16);
        toggleRegisterWhenClockDividesBy(7, 6);
        toggleRegisterWhenClockDividesBy(8, 12);

        //
        arraycopy(register, 9, register, 10, 30);

        //
        int x = 0;

        for (int t : theme) {
            x += register[t];
        }

        register[9] = x & 1;

        //
        return register[interval[0]] + (register[interval[1]] << 1) + (register[interval[2]] << 2) + (register[interval[3]] << 3);
    }

    private void toggleRegisterWhenClockDividesBy(int registerIndex, int divider) {
        if (tempoClock % divider == 0) {
            register[registerIndex] = register[registerIndex] == 1 ? 0 : 1;
        }
    }

    @Override
    public String toString() {
        StringBuilder str = new StringBuilder()
                .append("OOCCCCCCCBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBB clock=").append(tempoClock).append("\n")
                .append("FN½1248361234567890123456789012345678901\n");

        for (int bit : register) {
            str.append(bit == 1 ? "#" : "_");
        }

        return str.toString();
    }
}
